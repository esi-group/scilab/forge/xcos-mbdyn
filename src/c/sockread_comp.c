/* $Header: /var/cvs/mbdyn/mbdyn/mbdyn-1.0/contrib/ScicosInterface/sockread_comp.c,v 1.5 2011/01/23 11:16:33 masarati Exp $ */
/* 
 * MBDyn (C) is a multibody analysis code. 
 * http://www.mbdyn.org
 *
 * Copyright (C) 1996-2011
 *
 * Pierangelo Masarati	<masarati@aero.polimi.it>
 * Paolo Mantegazza	<mantegazza@aero.polimi.it>
 *
 * Dipartimento di Ingegneria Aerospaziale - Politecnico di Milano
 * via La Masa, 34 - 20156 Milano, Italy
 * http://www.aero.polimi.it
 *
 * Changing this copyright notice is forbidden.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation (version 2 of the License).
 * 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/*
 * Author: Tommaso Solcia <tommaso.solcia@mail.polimi.it>
 */

#ifdef _MSC_VER
#include <WinSock2.h>
#else
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <sys/types.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <signal.h>

#include <machine.h>
#include <math.h>

#include <scicos.h>
#include <scicos_block4.h>
#include <MALLOC.h>

/* outputs */
#define out(i)        ((GetRealOutPortPtrs(block, i+1))[0])

/* workspace */
#define workspace     (GetWorkPtrs(block))

/* parameters */
#define IparPtrs      GetIparPtrs(block)
#define hostname      Getint8OparPtrs(block, 1)

#define PORT          IparPtrs[0]
#define N_CH          IparPtrs[1]

#define DEBUG 1

void sockread(scicos_block *block, int flag)
{
  struct sockaddr_in server_addr;
  struct hostent *host = NULL;
#ifdef _MSC_VER
  SOCKET sockfd, *p_sock = NULL;
#else
  int sockfd, *p_sock = NULL;
#endif
  int numbytes, k;
  static double * num_read = NULL;

  /* Initializing block */
  if (flag == 4) 
    {
#ifdef DEBUG
      printf("DEBUG: read - flag 4 - init - N_CH = %d\n",N_CH);
#endif
      num_read = (double *)MALLOC(N_CH*sizeof(double));
#ifdef _MSC_VER
      p_sock = (SOCKET *)MALLOC(sizeof(SOCKET));
#else
      p_sock = MALLOC(sizeof(int));
#endif

      if (p_sock == NULL) 
	{
#ifdef DEBUG
	  printf("DEBUG: read - flag 4 - p_sock error\n");
#endif
	  //set_block_error(-16);
	}
      workspace = p_sock;
      
      /* make a socket: */
      sockfd = socket(PF_INET, SOCK_STREAM, 0);

#ifdef DEBUG
      printf("DEBUG: read - flag 4 - sockfd = %d\n",sockfd);
#endif

      if (sockfd == -1) 
	{
#ifdef DEBUG
	  printf("DEBUG: read - flag 4 - socket error\n");
#endif
	  perror("socket in function sockread");
	  //set_block_error(-3);
	}
      *p_sock = sockfd;

      /* get the host by name */
      host = gethostbyname(hostname);
      if (host == NULL) 
	{
#ifdef DEBUG
	  printf("DEBUG: read - flag 4 - host == NULL\n");
#endif
	  printf("ERROR, no such host\n");
	  FREE(p_sock);
	  FREE(num_read);
	  //set_block_error(-3);
	}

      /* make the address structure */
      server_addr.sin_family = AF_INET;
      server_addr.sin_port   = htons(PORT);
      server_addr.sin_addr   = *((struct in_addr *)host->h_addr);
      memset(&(server_addr.sin_zero), 0, 8);

#ifdef DEBUG
    printf("DEBUG: read - flag 4 - before connect\n");
#endif

      /* connect it to the server through the port we passed
       * in serv_addr.sin_port: */
      if (connect(sockfd, (struct sockaddr *)&server_addr,sizeof(struct sockaddr)) == -1)
	{
#ifdef DEBUG
	  printf("DEBUG: read - flag 4 - connect error\n");
#endif
	  perror("connect in function sockread");
	  //set_block_error(-3);
	}
#ifdef DEBUG
    printf("DEBUG: read - flag 4 - after connect\n");
#endif
    } 
  else if (flag == 1) 
    {
      /* Updating output */
      p_sock = workspace;
      
      sockfd = *p_sock;
      
#ifdef _MSC_VER
      numbytes = recv(sockfd, (void *)num_read, N_CH*sizeof(num_read), (MSG_WAITALL));
#else
      numbytes = recv(sockfd, (void *)num_read, N_CH*sizeof(num_read), (MSG_NOSIGNAL|MSG_WAITALL));
#endif
      if (numbytes == -1) 
	{
#ifdef DEBUG
	  printf("DEBUG: read - flag 1 - recv error\n");
#endif
	  perror("recv in function sockread");
	  //set_block_error(-3);
	}
      
      for (k = 0; k < N_CH; k++) 
	{
	  out(k) = num_read[k];
	}
      
    }
  else if (flag == 5)
    {
      /* Ending */
      p_sock = workspace;
      
#ifdef DEBUG
      printf("DEBUG: read - flag 5\n");
#endif
      sockfd = *p_sock;
#ifdef _MSC_VER
      closesocket(sockfd);
#else
      close(sockfd);
#endif
	    
      FREE(num_read);
      FREE(p_sock);
    }
}

